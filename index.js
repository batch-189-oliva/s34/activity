const express = require('express')

const app = express()

const port = 3000

app.use(express.json())
app.use(express.urlencoded({extended: true}))


// HOMEPAGE
app.get('/home', (request, response) => {
	response.send('Welcome to the home page')
});


// USERSPAGE
let users = [
	{
		"username": "johndoe",
		"password": "johndoe1234"
	},
	{
		"username": "doejohn",
		"password": "doejohn1234"
	}
]

app.get('/users', (request, response) => {
	response.send(users)
});


// DELETE-USERPAGE
app.delete('/delete-user', (request, response) => {
	for (let i = 0; i< users.length; i++) {
		if(users[i].username == request.body.username){
			users.splice(i, 1)

			response.send(`User ${request.body.username} has been deleted.`)

			break

		} else {
			response.send('User does not exist!')
		}
	} 
	
});


app.listen(port, () => console.log(`Server is running at port ${port}`))